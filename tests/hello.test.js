import assert from 'assert';

function sayHello(to = 'World'){
	let name = to;
	return `Hello, ${name}!`;
}

describe('say hello', function(){
	it('says hello with default value', function(){
		assert.equal(sayHello(), 'Hello, World!');
	});

	it('says hello with input value', function(){
		assert.equal(sayHello('Jane'), 'Hello, Jane!');
	});
});